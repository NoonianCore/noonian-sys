function ($http, $q) {

	return {
		getParameter: function(paramKey) {
			var deferred = $q.defer();

			$http.get('config/param/'+paramKey)
				.then(function(resp) {
					var responseObj = angular.fromJson(resp.data);
					if(responseObj.error) {
						deferred.reject(responseObj.error);
					}
					else {
						deferred.resolve(responseObj.result);
					}
				},
				function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		},
		
		customizeParameter: function(paramKey, value) {
		    var deferred = $q.defer();

			$http.post('config/customizeParam/'+paramKey, {value:value})
				.then(function(resp) {
				    // console.log('customizeParam result', data);
					var responseObj = angular.fromJson(resp.data);
					if(responseObj.error) {
						deferred.reject(responseObj.error);
					}
					else {
						deferred.resolve(responseObj.result);
					}
				},function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		    
		}

	};

}