function (NoonWebService, $q) {
    var db;
    
    /**
     * Constructor for type descriptor maps: special dynamic versions of BO definitions
     * @constructor
     * @param typeDescMap
     * @param {!Object.<string, Object>} fieldLabels
     **/
    var BusinessObjectLabelGroup = function(typeDescMap, fieldLabels, myClass, myLang) {
        if(!this || !(this instanceof BusinessObjectLabelGroup)) {
            return new BusinessObjectLabelGroup(typeDescMap, fieldLabels, myLang);
        }
        this.__myClass = myClass;
        this.__typeDescMap = typeDescMap;
        this.__langCache = {};
        
        this._setLabels(fieldLabels, myLang);
    };
    
    BusinessObjectLabelGroup.prototype._setLabels = function(fieldLabels, lang) {
        
        this.__langCache[lang || 'default'] = fieldLabels;
        _.assign(this, fieldLabels);
        
        var myLabels = this;
        
        //Process any composites; recursively instantiate BOLabelGroups as appropriate
        _.forEach(this.__typeDescMap, (td, fieldName)=> {
            var myTd = td instanceof Array ? td[0] : td;
            if(myTd.is_composite || myTd.type === 'composite') {
                var subLabelgroupKey = '#'+fieldName;
                if(
                    myTd.type_desc_map && myLabels[subLabelgroupKey] && 
                    !(myLabels[subLabelgroupKey] instanceof BusinessObjectLabelGroup)
                ) {
                    myLabels[subLabelgroupKey] = new BusinessObjectLabelGroup(myTd.type_desc_map, myLabels[subLabelgroupKey], lang, this.__myClass+'#'+fieldName);
                }
            }
        });
        
    };
    
    /**
     * @private 
     * getTypeDescriptor may need access to db when traversing into reference fields.  
     * However, simply injecting db as a factory causes a circular depenency.
     */
    BusinessObjectLabelGroup._init = function(dbService) {
        db = dbService;
    };
    
    /**
     * @function BusinessObjectLabelGroup#getLabel
     * Retrieves a label for a particular field/sub-field; recursing into composites and references if needed.
     * @param {string} path can be a simple fieldname or dotted into reference or composite fields, e.g.:
     *     db.SomeBusinessObj._bo_meta_data.field_labels.getTypeDescriptor('refField.blah');
     **/
    Object.defineProperty(BusinessObjectLabelGroup.prototype, 'getLabel', {
        enumerable:false, writable:false,
        value:function(path, prefixed, specialLabel, dontFallback) {
            var dotPos = path.indexOf('.');
            
            if(dotPos === -1) { //no dot -> just a field name
                if(specialLabel && this[specialLabel] && this[specialLabel][path]) {
                    return this[specialLabel][path];
                }
                else if(specialLabel && dontFallback) {
                    return null;
                }
                return this[path] || path;
            }
            
            //Peel off the first piece.
            var localField = path.substring(0, dotPos);
            var subPath = path.substring(dotPos+1);
            
            var localTd = this.__typeDescMap[localField];
    
            if(!localTd) {
              console.error('invalid fieldname for td', localField, this);
              return path;
            }
            
            if(localTd instanceof Array && localTd.length > 0) {
                localTd = localTd[0];
            } 
            
            let prefix = '';
            if(prefixed) {
                prefix = this[localField] || localField;
                prefix += '.';
            }
            
            if(localTd.type === 'reference') {
                //Grab label it from referenced db model
                var RefModel = db[localTd.ref_class];
                if(!RefModel) {
                    console.error('invalid reference class in type descriptor:', this);
                    return path;
                }
                
                return prefix+RefModel._bo_meta_data.field_labels.getLabel(subPath, prefixed, specialLabel, dontFallback);
            }
            else if(localTd.is_composite || localTd.type === 'composite') {
                var subLabelgroupKey = '#'+localField;
                
                if(!this[subLabelgroupKey]) {
                    console.error('labelgroup missing for composite field', this);
                    return path;
                }
                
                return prefix+this[subLabelgroupKey].getLabel(subPath, prefixed, specialLabel, dontFallback);
            }
            else {
              //dotted into a non-reference/composite or a non-existent field:
              console.log('invalid field specifier for this label group', this, subPath);
              return path;
            }
    
          } //end function
      });
      
      Object.defineProperty(BusinessObjectLabelGroup.prototype, 'getAbbreviatedLabel', {
        enumerable:false, writable:false,
        value:function(path, prefixed) {
            return this.getLabel(path, prefixed, '_abbreviated');
        }
      });
      
      Object.defineProperty(BusinessObjectLabelGroup.prototype, 'getExpandedLabel', {
        enumerable:false, writable:false,
        value:function(path, prefixed) {
            return this.getLabel(path, prefixed, '_expanded');
        }
      });
      
      Object.defineProperty(BusinessObjectLabelGroup.prototype, 'getHelpText', {
        enumerable:false, writable:false,
        value:function(path) {
            return this.getLabel(path, false, '_help_text', true);
        }
      });
      
      Object.defineProperty(BusinessObjectLabelGroup.prototype, 'switchLanguage', {
         enumerable:false, writable:false,
         value:function(lang) {
             var cached = this.__langCache[lang || 'default'];
             if(cached) {
                this._setLabels(cached, lang);
                return $q.when(this);
             }
             return NoonWebService.call('sys/i18n/getBoLabelGroup', {className:this.__myClass, language:lang}).then(result => {
                 if(result && result.result) {
                     this._setLabels(result.result, lang);
                     return this;
                 }
             })
         }
      });
      
    return BusinessObjectLabelGroup;
}