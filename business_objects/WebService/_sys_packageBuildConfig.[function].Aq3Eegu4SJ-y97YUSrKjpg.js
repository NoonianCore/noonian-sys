function(db, Config, queryParams, nodeRequire, Q) {
    const setVal = queryParams.set;
    const poll = queryParams.poll;
    
    const CFG_KEY = 'sys.packageBuilding';
    const path = require('path');
    const fs = require('fs');
    
    return Config.getValue(CFG_KEY, {}).then(paramVal=>{
        
        if(poll) {
            return paramVal._enabled;
        }
        
        if(!setVal) {
            return paramVal ||  {_enabled:false};
        }
        
        if(setVal === 'false') {
            paramVal._enabled = false;
            return Config.saveValue(CFG_KEY, paramVal).then(()=>paramVal);
        }
        else {
            console.log('Enabling package build: '+setVal);
            return db.BusinessObjectPackage.findOne({key:setVal}).then(bop=>{
                if(!bop) {
                    throw 'Invalid package key: '+setVal;
                }
                
                paramVal._enabled = setVal;
                
                if(paramVal[setVal]) {
                    return Config.saveValue(CFG_KEY, paramVal).then(()=>paramVal);
                }
                else {
                    
                    paramVal[setVal] = {
                        enableFilesystemSync: true,
                        syncUpdateLog: false
                    };
                    
                    return Config.saveValue(CFG_KEY, paramVal).then(()=>{
                        //Check if package contents need to be exported to the filesystem
                        const pkgPath = path.resolve(Config.serverConf.instanceDir, bop.key);
                        
                        try {
                            fs.readdirSync(pkgPath);
                        }
                        catch(err) {
                            const fsPackageSyncer = nodeRequire('../api/datasource/packaging/fs_sync.js');
                            
                            return fsPackageSyncer.packageObjectsToFs(bop, pkgPath)
                                .then(()=>fsPackageSyncer.writeMetaFile(pkgPath, bop))
                                .then(()=>paramVal)
                        }
                        
                        return paramVal;
                    });
                    
                }
            })
        }
        
    });
    
    
    
}