function(queryParams, semver) {
    
    let ver = queryParams.version;
    let inc = queryParams.inc;
    let pre = queryParams.prerelease == 'true';
    let meta = queryParams.meta;
    
    let sv = new semver(ver);
    
    
    if(inc) {
        //is it already a pre-release version?
        if(pre && sv.prerelease.length) {
            inc = 'pre';
        }
        else {
            inc = pre ? 'pre'+inc : inc;
        }
        sv.inc(inc);
        let ret = sv.toString();
        if(meta) {
            ret += '+'+meta;
        }
        return ret;
    }
    else {
        return JSON.stringify(sv);
    }
    
    
}