function (queryParams, db, NoonianPackaging) {
    var bopId = queryParams.id;
    var version = queryParams.version;
    if(!bopId) {
        throw 'invalid package id';
    }
    
    console.log('build package %s ...', bopId);
    return NoonianPackaging.generatePackage(bopId, version).then(function(result) {
        return {message:'created package '+result};
    });
}