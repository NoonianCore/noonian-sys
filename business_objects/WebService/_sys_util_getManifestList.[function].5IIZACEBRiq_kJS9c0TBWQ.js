function (db, httpRequestLib, _, queryParams) {
    // const pkgKey = 'app.dbui2';
    const pkgKey = queryParams.pkg;
    const boClass = queryParams.class;
    const boField = queryParams.field;
      
     return db.BusinessObjectPackage.findOne({key:pkgKey}).then(bop=>{
        const manifestMap = _.get(bop.manifest, boClass);
      
        return db[boClass].find({_id:{$in:Object.keys(manifestMap)}},{[boField]:1}).sort({[boField]:1}).then(boList=>_.map(boList, boField));
     });
     
    
    // var url = `http://gomashup.com/json.php?fds=geo/usa/zipcode/state/CA&jsoncallback=processResult`;
    // httpRequestLib.get({uri:url}, (err, httpResponse, body) =>{
    //         console.log(body);
    // });
}