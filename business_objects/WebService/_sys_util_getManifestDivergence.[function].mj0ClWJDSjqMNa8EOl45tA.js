function(db, _, Q, queryParams) {
    let ret = {messages:[]};
    
    const processObj = function(className, _id, ver) {
        if(!db[className]) {
            ret.messages.push('Bad className: '+className);
            return;
        }
        return db[className].findOne({_id}).then(bo=>{
            if(!bo) {
                ret.messages.push(`Missing object: ${className} ${_id}`);
                return;
            }
            if(bo.__ver !== ver) {
                ret[className] = ret[className] || [];
                ret[className].push(bo);
            }
        })
    };
    
    return db.BusinessObjectPackage.findOne({key:queryParams.key}, {manifest:1}).then(bop=>{
        if(!bop) {
            throw 'Bad BusinessObjectPackage: '+queryParams.key;
        }
        let manifest = bop.manifest;
        
        let promChain = Q(true);
        _.forEach(manifest, (idVerMap, className)=>{
            _.forEach(idVerMap, (ver, id)=> {
                promChain = promChain.then(processObj.bind(null, className, id, ver));
            });
        });
        
        return promChain.then(()=>ret);
        
    });
}