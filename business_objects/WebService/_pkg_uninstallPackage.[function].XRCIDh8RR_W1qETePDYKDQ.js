function (queryParams, db, NoonianPackaging) {
    var bopId = queryParams.id;
    var confirmed = (queryParams.confirmed === 'true');
    var keepData = (queryParams.keep_data === 'true');
    if(!bopId) {
        throw 'invalid package id';
    }
    
    if(!confirmed) {
        console.log('uninstall package pre-check: %s', bopId);
        return NoonianPackaging.preUninstall(bopId);
    }
    else {
        console.log('CONFIRMED UNINSTALL package: %s', bopId);
        return NoonianPackaging.uninstall(bopId, keepData);
    }
    
}