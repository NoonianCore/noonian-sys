function(queryParams, db, _, Q) {
    const reassignMap = JSON.parse(queryParams.reassign);
    
    console.log('reassignMap', reassignMap);
    
    const byPkg = {};
    
    _.forEach(reassignMap, (targetPkg, objId)=>{
        byPkg[targetPkg] = byPkg[targetPkg] || [];
        byPkg[targetPkg].push(objId);
    });
    
    var promChain = Q(true);
    var updates = [];
    
    const processUls = function(ids, pkg) {
        
        let bopProm = pkg ? db.BusinessObjectPackage.findOne({key:pkg}, {_id:1}) : Q(null);
        
        return bopProm.then(bop=>{
            let pkgRef = bop && {_id:bop._id, _disp:pkg} || null;
            
            return db.UpdateLog.update({object_id:{$in:ids}, incorporated:null}, {package:pkgRef}, {multi:true})
                .then(r=>updates.push(r));
        });
    };
    
    
    _.forEach(byPkg, (ids, pkg)=>{
        promChain = promChain.then(processUls.bind(null,ids,pkg));
    });
    
    return promChain.then(()=>{updates});
    
    // return db.UpdateLog.update({_id:{$in:ids}}, {package:null}, {multi:true});
    
}