function(db, queryParams, _, PromiseUtil) {
    const key = queryParams.key;
    
    
    return db.BusinessObjectPackage.findOne({key}).then(bop=>{
        return bop.getAugmentedManifest();
    });
}