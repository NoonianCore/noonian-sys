function(db, res, queryParams) {
    
    const moment = require('moment');
    
    const className = queryParams.class_name;
    const id = queryParams.id;
    var query = queryParams.query;
    
    if(!className || !(id||query)) {
        throw 'Missing required param(s)';
    }
    
    if(!db[className]) {
        throw 'Invalid BusinessObject class';
    }
    
    
    
    if(query) {
        try {
            query = JSON.parse(query);
        }
        catch(err) {
            throw 'Invalid query JSON';
        }
    }
    else {
        query = {_id:id}
    }
    
    var specifier = id ? '.'+id : '';
    var fileName = `${className}${specifier}.noonianExport.json`;
    
    res.attachment(fileName);
    
    return db[className].find(query).then(objects=>{
        return {
            timestamp:moment().format(), className, objects
        }
    });
    
}