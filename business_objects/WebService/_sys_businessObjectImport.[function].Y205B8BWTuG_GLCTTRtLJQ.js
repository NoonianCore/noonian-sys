function(db, req, _, Q) {
    
    const PkgService = db._svc.PackagingService;
    
    const multiparty = require('multiparty');
    const form = new multiparty.Form();
    
    const deferred = Q.defer();
    
    const importResult = {
        conflicts:[],
        imported:[]
    };
    const importObject = function(className, obj) {
        return PkgService.importObject(className, obj).then(result=>{
            importResult.imported.push(obj._id);
            if(result._bo_meta_data.class_name === 'PackageConflict') {
                importResult.conflicts.push(result);
            }
        });
    };
    
    const performImport = function(exportJson) {
        if(!exportJson || !exportJson.objects || !exportJson.className) {
            throw 'Invalid export';
        }
        importResult.className = exportJson.className;
        var importCount = 0;
        var promiseChain = Q(true);
        _.forEach(exportJson.objects, obj=>{
            promiseChain = promiseChain.then(importObject.bind(null, exportJson.className, obj));
        });
        return promiseChain.then(()=>importResult);
    }
    
    form.on('error', function(err) {
        console.error(err);
        deferred.reject(err);
    });
    
    form.on('part', function(part) {
        if(part.name === 'exportFile') {
            
            let fullJson = '';
            
            part.setEncoding('utf8');
            part.on('data', chunk=>{fullJson+=chunk});
            part.on('end', ()=>{
                const data = JSON.parse(fullJson);
                
                deferred.resolve(performImport(data));
            });
            
        }
        else {
            console.error('unknown part');
        }
        
    });
    
    
    form.parse(req);
    
    return deferred.promise;
    
}