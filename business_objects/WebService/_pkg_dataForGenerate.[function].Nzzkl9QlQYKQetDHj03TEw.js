function(queryParams, db, _, Q, Config) {
    const id = queryParams.id;
    
    const queryObj = {
        incorporated: {
            $empty: true
        },
        'package._id': id
    };
    
    
    
    return Q.all([
        db.BusinessObjectPackage.findOne({_id:id}),
        db.UpdateLog.find(queryObj).sort({timestamp:1}),
        Config.getValue('sys.packageBuilding', false)
    ])
    .spread((pkg, ulList, buildConfig)=>{
        
        if(!pkg) {
            throw 'invalid package id';
        }
        
        const manifest = pkg.manifest || {};
        
        const ret = {updates:[], bop:pkg, buildConfig};
        
        const promises = [];
        
        const handleObj = function(uls, oid) {
            let first = uls[0];
            let last = uls[uls.length -1];
            
            let oc = last.object_class;
            
            promises.push(
                db[oc].findOne({_id:oid}, {__ver:1}).then(obj=>{
                    if(!obj) {
                        console.error(`Missing ${oc} ${oid}`);
                        return;
                    }
                    ret.updates.push({
                        id:oid,
                        
                        boClass:last.object_class,
                        disp:last.object_disp,
                        
                        isCreate:(first.update_type === 'create'),
                        
                        manifestVersion:(manifest[oc] && manifest[oc][oid]),
                        latestVersion:obj.__ver,
                        
                        updateCount:uls.length,
                        updateLog:uls
                    });
                })
            );
        }
        
        const objMap = {};
        _.forEach(ulList, ul=>{
            let oid = ul.object_id;
            objMap[oid] = objMap[oid] || [];
            objMap[oid].push(ul);
        });
        
        _.forEach(objMap, handleObj);
        
        return Q.all(promises).then(()=>ret);
    });
}