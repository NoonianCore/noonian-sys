function(scheduler, queryParams) {
    if(!queryParams.id) {
        throw 'missing required parameter';
    }
    return scheduler.manualExecute(queryParams.id).then(()=>{
        return {
            message:'Manual execution succeeded', 
            action:{
                broadcast:'dbui.refresh',
                params:{id:queryParams.id}
            }
        }
    })
}