function (value, typeDesc) {
    if(typeDesc.hash && !(value && value.matches)) {
        var bcrypt = require('bcryptjs');
        value = value || {};
        
        
        const matches = function(toCheck) {
          return bcrypt.compareSync(toCheck, this.hash);  
        };
        
        Object.defineProperty(value, 'matches', {
            enumerable:false,writable:false,
            value:matches
        });
    
    }
    
    return value;
}