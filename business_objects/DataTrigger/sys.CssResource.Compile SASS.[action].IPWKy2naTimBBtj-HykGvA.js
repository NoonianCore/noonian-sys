function (nodeRequire, Q) {
    if(
        (this.preprocessor === 'sass' || this.preprocessor === 'scss') &&
        this.content
    ) {
        var sass = nodeRequire('sass');
        
        try {
            var result = sass.compileString(this.content);
            if(result && result.css) {
                this.compiled_content = result.css;
            }
            if(result && result.error) {
                throw new Error(result.error);
            }
        }
        catch (err) {
            console.error(err.message);
            console.error(err);
        }
    }
}