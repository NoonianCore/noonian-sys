function() {

    
    const named = {
        month:["?","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
        day_of_week:["Sun","Mon","Tue","Wed","Thu","Fri","Sat","Sun"]
    };
    
    const labels = {
        second:'sec',
        minute:'min',
        hour:'hour',
        day_of_month:'day of month',
        month:'month',
        day_of_week:'day of week'
    };
    
    const ordinalSuffix = function(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    };
    
    const pad = n=>(n < 10 ? '0'+n : n);
    
    const justNum = /^\d+$/;
    const recurring = /\*\/(\d+)/;
    const analyzePiece = function(x, field) {
        // * - wildcard
        // * / n - every n seconds/minutes/hours
        // a-b range of values between a and b
        // a,b,c discreet values a,b,c (can be combined w/ range)

        if(x === '*') {
            return {wildcard:true, desc:'any '+labels[field]};
        }
        
        let n = justNum.exec(x);
        if(n) {
            
            if(named[field]) {
                x = named[field][n];
            }
            return {
                isNumber:true,
                number:x,
                desc:labels[field]+': '+x
            }
        }
        
        let r = recurring.exec(x);
        if(r) {
            return {
                recurring:true,
                desc:'every '+ordinalSuffix(r[1])+' '+labels[field]
            };
        }
        
        return {
            desc:labels[field]+'s: '+x //ranges or discreet values
        }
    };
    
    const summarize = function(s) {
            
        // second minute hour
        // day_of_month  month day_of_week
        
        const a = {
            second:analyzePiece(s.second, 'second'),
            minute:analyzePiece(s.minute, 'minute'),
            hour:analyzePiece(s.hour,'hour'),
            day_of_month:analyzePiece(s.day_of_month,'day_of_month'),
            month:analyzePiece(s.month,'month'),
            day_of_week:analyzePiece(s.day_of_week,'day_of_week')
        };
        
        var parts = [];
        
        if(a.hour.isNumber) {
            var time = a.hour.number+':';
            var suffix = [];
            if(a.minute.isNumber) {
                time += pad(a.minute.number);
            }
            else if(a.minute.wildcard) {
                time += '00';
                suffix.push(a.minute.desc);
            }
            
            if(a.second.isNumber) {
                time += ':'+pad(a.second.number);
            }
            else if(a.second.desc) {
                suffix.push(a.second.desc);
            }
            if(suffix.length) {
                time += ' at '+suffix.join(', ');
            }
            parts.push(time);
        }
        else {
            let p = [a.hour.desc,a.minute.desc,a.second.desc].join(', ');
            parts.push(p);
        }
        parts.push(a.day_of_week.desc);
        parts.push(a.month.desc);
        parts.push(a.day_of_month.desc);
        
        return parts.join('\n');
    };
    
    this.sched_summary = summarize(this.schedule)
    
}