function (_) {
    const def = this.definition;
    var doc = this.doc || '';
    
    //First, scan through the definition to replace string types w/ proper type descriptor objects
    _.forEach(def, (td, fieldName) => {
        if(fieldName.indexOf('_') !== 0) {
            if(typeof td === 'string') {
                def[fieldName] = {type:td}
            }
            else if(td instanceof Array && td.length && typeof td[0] === 'string') {
                def[fieldName] = [{type:td[0]}];
            }
        }
    });
    
    //Next, scan thought the doc and add placeholder @property lines for each field
    const regex = /^\s*@property {(.+)}\s*(\S+).*$/;
    
    const foundNames = {};
    
    const lines = doc.split('\n');
    _.forEach(lines, function(line) {
        var result = regex.exec(line);
        console.log(result);
        if(result) {
            foundNames[result[2]] = true;
        }
    });
    
    const fieldsToAdd = [];
    
    _.forEach(def, function(td, fieldName) {
        if(fieldName.indexOf('_') !== 0 && !foundNames[fieldName]) {
            fieldsToAdd.push(fieldName);
        }
    });
    
    if(fieldsToAdd.length) {
        fieldsToAdd.sort();
        doc += '\n';
        
        _.forEach(fieldsToAdd, function(fieldName) {
            var td = def[fieldName];
            var isArray = td instanceof Array;
            if(isArray) {
                td = td[0];
            }
            var type = td.type;
            if(type === 'reference' && td.ref_class) {
                type += '.<'+td.ref_class+'>';
            }
            if(isArray) {
                type += '[]';
            }
            doc += ' @property {'+type+'} '+fieldName+' \n';
        });
        
        this.doc = doc;
    }
    
}