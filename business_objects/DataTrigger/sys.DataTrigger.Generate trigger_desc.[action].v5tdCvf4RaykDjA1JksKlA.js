function(_) {
    
    const c = this.on_create, u = this.on_update, d = this.on_delete;
    
    let encoded = 0;
    
    c && (encoded += 4);
    u && (encoded += 2);
    d && (encoded += 1);
    
    
    if(!encoded) {
        this.trigger_desc = 'Disabled';
        return;
    }
    
    
    const eventMap = [
        '',             // 000 
        'delete',       // 001 d
        'update',       // 010 u
        'update/delete',// 011 ud
        'create',       // 100 c
        'create/delete',// 101 cd
        'save',         // 110 cu
        'any change'    // 011 cud
    ];
    
    this.trigger_desc = _.startCase(this.before_after)+' '+eventMap[encoded];
}