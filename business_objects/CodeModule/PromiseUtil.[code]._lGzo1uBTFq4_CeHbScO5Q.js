function(Q) {
    
    const Invoker = function(obj, memberFunctionName, args) {
        this.obj = obj;
        this.fnName = memberFunctionName;
        this.args = args;
    };
    Invoker.prototype.invoke = function() {
        return this.obj[this.fnName].apply(this.obj, this.args);
    };
    Invoker.prototype.get = function() {
        return this.invoke.bind(this);
    }
    
    
    const Chain = function() {
        this.promise = Q(true);
        this.count = 0;
    };
    
    Chain.prototype.add = function(fn, errFn) {
        this.promise = this.promise.then(fn, errFn);
        this.count++;
    };
    
    Chain.prototype.addInvoker = function(obj, memberFunctionName, args) {
        let invoker = new Invoker(obj, memberFunctionName, args);
        this.add(invoker.get());
    };
    
    
    return {
        Invoker,
        Chain
    };
}