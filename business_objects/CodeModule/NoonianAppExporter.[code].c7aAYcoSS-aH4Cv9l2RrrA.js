function (db, Q, _) {
    const fs = require('fs');
    
    const exports = {};
    
    
    const mkdirRecursive = function(dir) {
        const dirs = dir.split('/');
        if(dirs[0] === '') {
            dirs.splice(0, 1);
            dirs[0] = '/'+dirs[0];
        }
        
        var build = '';
        for(var i=0; i < dirs.length; i++) {
            build += dirs[i];
            if(!fs.existsSync(build)) {
                fs.mkdirSync(build);
            }
            build += '/';
        }
    };
    
    const exportModule = 
    exports.exportModule = function(moduleId, outputDir, exportDeps) {
        //Serve content to file output stream
        //  this will include all providers in one file.  TODO optionally split out according to "path" field
        var modObj; 
        
        var webResourceQuery = null;
        if(exportDeps) {
            //list of references to webResoures
            const wrIds = _.pluck(exportDeps, '_id');
            webResourceQuery = db.WebResource.find({_id:{$in:wrIds}}).exec();
        }
        
        return Q.all([
            db.AngularModule.findOne({_id:moduleId}).exec(),
            db.AngularTemplate.find({'module._id':moduleId}).exec(),
            webResourceQuery
        ])
        .then(function([modObj, templates, otherResources]) {
            
            var modOutDir = outputDir+'/'+modObj.path;
            mkdirRecursive(modOutDir);
            
            var outStream = fs.createWriteStream(modOutDir+'/'+modObj.name);
            
            var depPromises = [modObj.serveContent(outStream)];
            
            
            const writeWebResource = wrObj=>{
                var tempOutDir = outputDir+'/'+wrObj.path;
                mkdirRecursive(tempOutDir);
                
                var outStream = fs.createWriteStream(tempOutDir+'/'+wrObj.name);
                
                var contentType = typeof wrObj.content;
                var content = wrObj.content;
                if(contentType === 'function') {
                    content = ''+content;
                }
                else if(contentType === 'object'){
                    content = JSON.stringify(content);
                }
                
                outStream.write(content);
                outStream.end();
            };
            
            //Next, take care of all templates belonging to this module
            _.forEach(templates, writeWebResource);
            
            //And other WebResources
            _.forEach(otherResources, writeWebResource);
            
            
            
            //Recursively generate mod_dependencies
            _.forEach(modObj.mod_dependencies, depRef => {
                depPromises.push(exportModule(depRef._id, outputDir));
            }); 
            
            return Q.all(depPromises);
        });
        
    };

    const exportApp = 
    exports.exportApp = function(appId, outputDir) {
        
        var appObj;
        //First, AngularApp itself:
        return db.AngularApp.findOne({_id:appId}).then(function(r) {
            appObj = r;
            
            var appOutDir = outputDir+'/'+appObj.path;
            mkdirRecursive(appOutDir);
            
            var outStream = fs.createWriteStream(appOutDir+'/'+appObj.name);
            
            var promises = [appObj.serveContent(outStream, minify)];
            
            //Then, module and dependencies:
            if(appObj.module && appObj.module._id) {
                promises.push( exportModule(appObj.module._id, outputDir) );
            }
            
            return Q.all(promises);
        })
        .then(function() {
            var jsIds = _.pluck(appObj.js_dependencies, '_id');
            if(jsIds.length) {
                return db.JsResource.find({_id:{$in:jsIds}}).then(function(jsResourceList) {
                    _.forEach(jsResourceList, function(jr) {
                        if(jr.content) {
                            var myOutDir = outputDir+'/'+jr.path;
                            mkdirRecursive(myOutDir);
                            var outStream = fs.createWriteStream(myOutDir+'/'+jr.name);
                            jr.serveContent(outStream);
                        }
                    });
                });
            }
        })
        .then(function() {
            var cssIds = _.pluck(appObj.css_dependencies, '_id');
            if(cssIds.length) {
                return db.CssResource.find({_id:{$in:cssIds}}).then(function(cssResourceList) {
                    _.forEach(cssResourceList, function(cr) {
                        if(cr.content) {
                            var myOutDir = outputDir+'/'+cr.path;
                            mkdirRecursive(myOutDir);
                            var outStream = fs.createWriteStream(myOutDir+'/'+cr.name);
                            cr.serveContent(outStream);
                        }
                    });
                });
            }
        })
        .then(function() {
            return {result:'success'};
        })
        ;
        
    };

    return exports;
}