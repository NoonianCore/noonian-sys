function(db, Q, _, logger, auth, I18n, nodeRequire) {
    
    const exports = {};
    
    //when this key is found in a config, it's value specifies a LabelGroup and subkey
    // to be resolved to an internationalized string upon customization
    const LABELGROUP_REF_KEY = '$i18n';
    
    logger = logger.get('sys.config');
    
    exports.serverConf = nodeRequire('../conf');
    
    
    const findLgRefs = function(obj, currPath, result) {
        currPath = currPath || [];
        result = result || {};
        
        Object.keys(obj).forEach(k=>{
            let v = obj[k];
            if(k === LABELGROUP_REF_KEY) {
                result[currPath.join('.')] = v;
            }
            else if(v && v instanceof Object) {
                currPath.push(k);
                findLgRefs(v, currPath, result);
                currPath.pop();
            }
        });
        return result;
        
    };
    
    const processLgRefs = function(val, theUser) {
        
        //Before returning it, check for $i18n
        var promChain = Q(true);
        if(val) {
            let lgRefs = findLgRefs(val);
            _.forEach(lgRefs, (ref, path)=>{
                promChain = promChain.then(()=>{
                  return I18n.resolveLgRef(ref, theUser).then(
                      r=>_.set(val, path, r),
                      err=>_.set(val, path, err)
                    );
                })
            })
        }
        
        return promChain.then(()=>val);
    };
    
    exports.getValue = 
    exports.getParameter = function(key, defaultValue) {
      const deferred = Q.defer();
    
      db.Config.find({key:key, user:null}).exec().then(
        function(result) {
    
          if(result && result.length > 0) {
            deferred.resolve(processLgRefs(result[0].value));
          }
          else if(defaultValue != undefined) {
            deferred.resolve(defaultValue);
          }
          else {
            deferred.reject(key+' not found');
          }
        },
        function(err) {
          deferred.reject(err);
        }
      );
    
      return deferred.promise;
    
    };
    
    exports.saveValue = 
    exports.saveParameter = function(key, value, userId) {
    
      logger.debug('saveValue(%s, %j, %s)', key, value, userId);
    
      const query = {key:key};
      
      if(userId) {
        query['user._id'] = userId;
      }
      else {
        query.$or = [{user:{$exists:false}},{user:null}];
      }
    
      return db.Config.findOne(query).exec()
        .then(function(result){
            const configObj = result || new db.Config({key:key});
            
            configObj.value = value;
            
            if(userId) {
                configObj.user = {_id:userId};
            }
            
            return configObj.save();
        });
    
    };
    
    
    exports.getCustomizedValue = 
    exports.getCustomizedParameter = function(key, userId, defaultValue) {
        var theUser;
        return db.User.findOne({_id:userId}).then(user=> {
            theUser = user;
        
            return db.Config.find({key:key, $or:[{user:{$exists:false}},{user:null},{'user._id':userId}]}).exec();
        })
        .then(result=> {
            
          
            if(result.length === 0) {
                return defaultValue;
            }
    
            var base, custom;
            _.forEach(result, configObj => {
                if(configObj.user) { 
                    custom = configObj;
                }
                else if(configObj.rolespec && configObj.rolespec.length) {
                    //a config object with a rolespec takes precidence over one without.
                    if(auth.checkRolesForUser(theUser, configObj.rolespec, true)) {
                        base = configObj;
                    }
                }
                else if(!base) {
                  base = configObj
                }
            });
    
            base = base ? base.value : null;
            custom = custom ? custom.value : null;
            
            if(base && custom) {
                //Merge custom atop base
                return _.merge(base, custom);
            }
            else {
                return base || custom;
            }

      })
      .then(retVal=>{
          //Before returning it, check for $i18n
          return processLgRefs(retVal, theUser);
      })
      ;
    
    };
    
    return exports;
}