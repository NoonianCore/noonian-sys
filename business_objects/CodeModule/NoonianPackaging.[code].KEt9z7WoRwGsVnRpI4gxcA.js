function (db, _, Q, Config, logger, JsonUtil, PromiseUtil, VersionId) {
    logger = logger.get('NoonianPackaging');
    
    const exports = {};
    
    const path = require('path');
    const fs = require('fs');
    
    const fsPromises = require('fs/promises');
    
    const semver = require('semver');
    
    const GridFsService = db._svc.GridFsService;
    const PkgService = db._svc.PackagingService;
    const mongoConn = db.mongooseConnections.system || db.mongooseConnections.default;
    
    const BUILD_CFG_KEY = 'sys.packageBuilding';
    
    const instanceDir = Config.serverConf.instanceDir;

	
	
// 	const isNewer = (a, b)=>(new semver(a).compare(b) > 0);
	
	/**
	 * Get metadata for all enabled remote repositories 
	 */
	const getRemoteMetadata =
	exports.getRemoteMetadata = function() {
	    return db.RemotePackageRepository.find({enabled:true}).then(rprList=>{
    		const promiseList = [];
    		_.forEach(rprList, rpr=>{
    			promiseList.push(rpr.getMetaData());
    		});
    
    		return Q.allSettled(promiseList).then(function(promises) {
    			var result = [];
    			_.forEach(promises, (p,i)=>{
    				if(p.state === 'fulfilled') {
    				    let rpr = rprList[i];
    					result.push({
        					repo:{name:rpr.name, _id:rpr._id, url:rpr.url},
        					metadata:p.value  
        				});
    				}
    			});
    			return result;
    		});
    	});
	};
	
	
	const ignoreDirs = {
        node_modules:true,
        client:true
    };
    const pluckDirs = 
        entries=>
            _.map(
                _.filter(entries, x=>(x.isDirectory() && !ignoreDirs[x.name])), 
                'name'
            );
	/**
	 * Look at subdirectories in instance directory for dev packages
	 */
	const getLocalDirectories = 
	exports.getLocalDirectories = function() {
	    
	    return fsPromises.readdir(instanceDir, {withFileTypes:true}).then(dirEntries=>{

	        const chain = new PromiseUtil.Chain();
	        const dirsToCheck = pluckDirs(dirEntries);
	        
	        const ret = [];
	        
	        const checkDir = function(dir) {

	            let pkgMetaPath = path.resolve(instanceDir, dir, 'pkg_meta.json');
	            return fsPromises.readFile(pkgMetaPath, 'UTF-8').then(contents=>{
                    var pkgMeta;
	                try {
	                    pkgMeta = JSON.parse(contents);
	                }
	                catch(err) {
	                    logger.error('Bad pkg_meta.json in subdirectory: '+dir);
	                    return;
	                }
	                
	                return db.BusinessObjectPackage.findOne({_id:pkgMeta._id}).then(bop=>{
	                    if(!bop) {
	                        pkgMeta.subdirectory = dir;
	                        ret.push(pkgMeta);
	                    }
	                });
	                
	                
	            },
	            err=>{logger.silly(err); logger.debug('Subdir not a noonian package: '+dir)}
	            );
	        };
	        
	        _.forEach(dirsToCheck, subdir=>chain.add(checkDir.bind(this, subdir)));
	        
	        
	        return chain.promise.then(()=>ret);
	    });
	    
	};
	
	exports.getLocalStatus = function() {
	    
        return Q.all([
            getRemoteMetadata(),
            Config.getValue(BUILD_CFG_KEY, {}),
            getLocalDirectories()
        ])
    	.spread((remoteResults, buildCfg, localDirs)=>{
    	    //Now, scan result object to mark status on each package
    	    return db.BusinessObjectPackage.find({}).lean().then(bopList=>{
    	        const installed = _.keyBy(bopList, 'key');
    	        const available = {};
    	        const repos = [];
    	        const devList = [];
    
    			_.forEach(remoteResults, rmtList => {
    			    let repo = rmtList.repo;
    			    repos.push(repo);
    			    
    			    available[repo._id] = [];
    			    
    			    let myAvailable = [];
    			    
    			    _.forEach(rmtList.metadata, md => {
    			        if(!md.version) {
    			            console.error('REMOTE PKG NEEDS UPDATE:\n'+JSON.stringify(md, null,3));
    			            return;
    			        }
    			        let k = md.key;
    			        let hostedVersion = new semver(md.version);
    			        let installedBop = installed[k];
    			        
    			        if(!installedBop) {
    			            myAvailable.push(md);
    			        }
    			        else {
    			            installedBop.repo = repo;
    			            installedBop.repo_version = hostedVersion.toString();
    			            
    			            let cmp = hostedVersion.compare(installedBop.version)
    			            
    			            installedBop.repo_version_diff = cmp;
    			            
    			        }
    			        
    			    });
    			    
    			    available[repo._id] = _.sortBy(myAvailable, 'key');
    			}); //end remoteResults iteration
    		    
    		    
    			Object.keys(buildCfg).forEach(k=>{
    			    let bop = installed[k];
    			    if(bop) {
    			        devList.push(bop);
		                bop.build_cfg = buildCfg[k];
		                if(buildCfg._enabled === k) {
		                    bop.build_enabled = true;
		                }
    			    }
    			});
    			
    			return {
    			    installed:bopList,
    			    dev:devList,
    			    available,
    			    repos,
    			    localDirs
    			};
    	    });
    	}) 
    	;
	};
	
	
	/**
	 * Utility to pull together id's and versions from package on filesystem
	 */
	exports.generateManifestFromFs = function(pkgKey) {
    
        const boDir = path.join(instanceDir, pkgKey, 'business_objects');
        
        const fileRegex = /^(?:.+)\.\[?\]\.(.{22})\.json$/; 
        
        const ret = {};
        
        
        _.forEach(fs.readdirSync(boDir), boClass=>{
            let classDir = path.join(boDir, boClass);
            
            let myMap = ret[boClass] = {};
            
            _.forEach(fs.readdirSync(classDir), fileName=>{
                if(fileRegex.test(fileName)) {
                    
                    let obj = JSON.parse(fs.readFileSync(path.join(classDir, fileName)));
                    
                    myMap[obj._id] = obj.__ver;
                }
            });
            
        });
        
        return ret;
	};
	
	exports.loadFromFs = function(subdir) {
	    const pkgDir = path.resolve(instanceDir, subdir);
        return PkgService.packageFromFs(pkgDir).then(bop=>{
            return PkgService.checkDependencies(bop).then(deps=>{
                return PkgService.installDependencies(bop, deps).then(depResults=>{
                    return {installed:bop, deps:depResults};
                });
            });
        });
    };
	
	exports.generatePackage = function(bopId, version) {
    
        var deferred = Q.defer();
        
        var bop;              //The BusinessObjectPackage object
        var mergedManifest;   //The updated manifest (that incorporates UpdateLog's)
        var pkgStream;        //The stream to which the package json is written.
        
        var abstractBods = {}; //tells us which BOD's in the packages are marked 'abstract' (need to output first)
        
        const incorporatedUpdateLogIds = []; //keep track 
        const deletes = {};
        
        //const isMarkedDeleted = (className, id)=>(deletes[className] || []).indexOf(id) > -1;
        
        //Phase 1: 
        //  grab the BusinessObjectPackage we're working with, and query for all of the UpdateLogs
        //  and use to pull together mergedManifest
        Q.all([
            db.BusinessObjectPackage.findOne({_id:bopId}),
            db.UpdateLog.find({package:bopId, incorporated:null}).sort({timestamp:-1})
        ])
        .then(function(resultArr) {
          
        bop = resultArr[0];
        var updateLogs = resultArr[1];
        
        mergedManifest = bop.manifest ? _.clone(bop.manifest) : {};    
        
        
        //Merge in UpdateLog records
        const manifestUpdates = {};
        
        _.forEach(updateLogs, function(ul) {
            incorporatedUpdateLogIds.push(ul._id);
            
            var forClass = manifestUpdates[ul.object_class];
            if(!forClass) {
                forClass = manifestUpdates[ul.object_class] = {};
            }
            if(!forClass[ul.object_id]) {
                if(ul.update_type !== 'delete') {
                    forClass[ul.object_id] = ul.updated_version;
                }
                else {
                    let delList = deletes[ul.object_class] = deletes[ul.object_class] || [];
                    delList.push(ul.object_id);
                    // forClass[ul.object_id] = 'deleted';
                }
            }
        });
        
        _.forEach(Object.keys(manifestUpdates), function(className) {
            if(!mergedManifest[className]) {
                mergedManifest[className] = {};
            }
            _.assign(mergedManifest[className], manifestUpdates[className]);
        });
        
        
        
        //Do a pass through the manifest to preprocess and ensure all version id's are up-to-date
        //  (because we're streaming the meta object first, which includes the manifest)
        var checkBo = function(className, boId) {
            
            var projection = {__ver:1};
            var bodCheck = false;
            if(className === 'BusinessObjectDef') {
                projection.abstract = 1;
                bodCheck = true;
            }
            
            return db[className].findOne({_id:boId}, projection).then(boStub=>{
                logger.silly('CHECKING %s.%s -> %j', className, boId, boStub);
                if(!boStub) {
                    logger.warn('BusinessObject %s.%s missing from DB but no "delete" UpdateLog was found', className, boId);
                    // mergedManifest[className][boId] = 'deleted'; //Presume deleted
                    let delList = deletes[className] = deletes[className] || [];
                    delList.push(boId);
                    return;
                }
                
                mergedManifest[className][boId] = boStub.__ver;
                if(bodCheck && boStub.abstract) {
                    abstractBods[boId] = true;
                }
            });
        };
        
        var versionCheckPromises = [];
        _.forEach(mergedManifest, function(idVerMap, className) {
            if(className.indexOf('_') === 0) 
                return;
             
            
            _.forEach(idVerMap, function(v, id) {
                
                if(db[className]) {
                  versionCheckPromises.push(
                      checkBo(className, id)
                  );
                }
                else {
                  logger.error('Missing model for className %s  (did you rename a BusinessObjectDef?)');
                }
                
            });
        }); //end forEach(mergedManifest)
        
        return Q.all(versionCheckPromises);
        
        }) //end initial BOP and UpdateLog lookup
        .then(function() {
        //Phase 2:
        //  mergedManifest is now fully up-to-date w/ system and
        //  abstractBods tells us which BusinessObjectDef's are marked as 'abstract' 
        //  Create a file in GridFs and start streaming
        
        mergedManifest._deleted = deletes;
        
        //The BOP object itself will act as our 'meta' block, 
        bop.manifest = mergedManifest;
        bop.markModified('manifest');
        bop.package_file = undefined;
        
        //Manage package-level versioning - default to increment minor version
        if(version) {
          bop.version = version;
        }
        else {
          let v = new semver(bop.version);
          v.inc('minor');
          bop.version = v.toString();
        }
        
        //Stub out the package_file field value (an "attachment")
        var attachmentMetaObj = {
          filename: bop.key+'.'+bop.version+'.json',
          type:'application/json'
        };
        
        
        //Start streaming to the output package
        pkgStream = GridFsService.writeFile(attachmentMetaObj);
        
        pkgStream.on('error', function(err) {
          deferred.reject(err);
        });
        
        
        //don't want to serialize __ver, but save it off
        var verOrig = bop.__ver;
        bop.__ver = undefined;
        
        pkgStream.write('{\n"metadata":');
        pkgStream.write(JsonUtil.stableStringify(bop));
        pkgStream.write(',\n"business_objects":[\n');
        
        var comma = ''; //prepended to each object as written; only blank for the first one
        
        
        bop.__ver = verOrig;
        bop.package_file = attachmentMetaObj;
        
        //Set up to find attachment refs to pull into package after BO's are processed
        const attachmentIds = [];
        const findAttachmentRefs = function(obj, tdMap) {
            _.forEach(tdMap, (td, f)=>{
                if(td.type === 'attachment' && obj[f] && obj[f].attachment_id) {
                    attachmentIds.push(obj[f].attachment_id);
                }
                else if(td.type === 'composite' && td.type_desc_map && obj[f]) {
                    findAttachmentRefs(obj[f], td.type_desc_map);
                }
            });
        };
        
        var filesCollection, chunksCollection;
        const streamAttachment = function(attId) {
            filesCollection = filesCollection || mongoConn.db.collection('fs.files');
            chunksCollection = chunksCollection || mongoConn.db.collection('fs.chunks');
            const fDeferred = Q.defer(), cDeferred = Q.defer();
            filesCollection.findOne({_id:attId}, (err,r)=>{
                err && fDeferred.reject(err);
                fDeferred.resolve(r);
            });
            chunksCollection.find({files_id:attId}).toArray((err, r)=>{
                err && cDeferred.reject(err);
                cDeferred.resolve(r);
            });
            return Q.all([
                fDeferred.promise, cDeferred.promise
            ])
            .spread((file, chunks)=>{
                var streamObj = {
                    file,
                    chunks
                };
                pkgStream.write(comma + JSON.stringify(streamObj));
                comma = ',\n'; 
            });
            
        };
        
        
        
        var streamBo = function(theObj) {
            if(!theObj) {
                logger.error('Trying to stream BO that wasnt in the system (Should never see this)');
                return;
            }
            var streamObj = theObj.toPlainObject();
            streamObj._class = theObj._bo_meta_data.class_name;
            delete streamObj.__v;
            pkgStream.write(comma + JsonUtil.stableStringify(streamObj));
            comma = ',\n';
            
            findAttachmentRefs(theObj, theObj._bo_meta_data.type_desc_map);
        };
        
        var promiseChain = Q(true);  //a chain of calls, alternating between findOne and streamBo
        var appendToChain = function(boClass, boId) {
            promiseChain = promiseChain
                .then(
                    db[boClass].findOne.bind(db[boClass], {_id:boId}, null, null, null) 
                )
                .then(
                    streamBo
                );
        };
        
        
        //Write BusinessObjectDef's first, with abstract ones at the start
        if(mergedManifest.BusinessObjectDef) {
            
            var allBodIds = Object.keys(mergedManifest.BusinessObjectDef);
            allBodIds.sort();
            
            _.forEach(allBodIds, function(bodId) {
                if(abstractBods[bodId] && mergedManifest.BusinessObjectDef[bodId] !== 'deleted') {
                    appendToChain('BusinessObjectDef', bodId);
                }
            });
            _.forEach(allBodIds, function(bodId) {
                if(!abstractBods[bodId] && mergedManifest.BusinessObjectDef[bodId] !== 'deleted') {
                    appendToChain('BusinessObjectDef', bodId);
                }
            });
        }
        
        //Now the rest of the objects, sorted alphabetically
        var allClasses = _.filter(Object.keys(mergedManifest), k=>(k.indexOf('_')!==0));
        allClasses.sort();
        _.forEach(allClasses, function(boClass) {
            if(boClass !== 'BusinessObjectDef') {
                var allIds = Object.keys(mergedManifest[boClass]);
                allIds.sort();
                
                _.forEach(allIds, function(id) {
                    if(mergedManifest[boClass][id] !== 'deleted') {
                        appendToChain(boClass, id);
                    }
                });
            }
        });
        
        //At this point, promiseChain holds the full sequence of calls to 
        // retrieve and stream each BO belonging to the package.
        
        
        //Next, let's check to see if any attachments need to be persisted
        promiseChain = promiseChain.then(function() {
            if(attachmentIds.length) {
                pkgStream.write('],\n"attachments":[');
                comma = '';
                
                var attachmentChain  = Q(true);
                attachmentIds.forEach(aid=>{
                    attachmentChain = attachmentChain.then(streamAttachment.bind(null, aid));
                });
                return attachmentChain;
            }
        });
        
        //When that's all done...
        promiseChain = promiseChain.then(function() {
            pkgStream.end(']}\n'); //close off business_object array, and initial open curlybrace
            logger.info('COMPLETED PACKAGE GENERATION for %j', bop.key);        
            return bop.save().then(function() {
              var incorpVer =bop.major_version+'.'+bop.minor_version;
              return db.UpdateLog.update({_id:{$in:incorporatedUpdateLogIds}}, {$set:{incorporated:incorpVer}}, {multi:true}).exec();
            });
        });
        
        return promiseChain;
        
        }) //end Phase 2
        .then(
        function() {
          logger.verbose('generated package file %s for %s ',bop.package_file.attachment_id, bop.key);
        
          let pkgBuildCfg = PkgService.pkgConfigMap && PkgService.pkgConfigMap[bop.key];
          
          if(pkgBuildCfg && pkgBuildCfg.enableFilesystemSync) {
            
            //Stream the new package to dist
            var targetPath = path.resolve(pkgBuildCfg.fs_base, 'dist');
            logger.verbose('syncing package to filesystem: %j', targetPath);
            
            fs.mkdirSync(targetPath, {recursive:true});
            
            return GridFsService.getFile(bop.package_file.attachment_id).then(function(gridfsFileObj) {
              logger.info('Exporting package %s to %s', gridfsFileObj.metadata.filename, targetPath);
        
              var outputFile = fs.createWriteStream(path.resolve(targetPath, gridfsFileObj.metadata.filename));
              gridfsFileObj.readstream.pipe(outputFile);
              
              var onComplete = function() {
                deferred.resolve(bop.package_file.attachment_id);
              };
              outputFile.on('finish', onComplete);
              outputFile.on('error', onComplete);
              
            }); 
          }
          else {
            deferred.resolve(bop.package_file.attachment_id);
          }
        },
        function(err) {
          deferred.reject(err);
        }
        );
        
        return deferred.promise;
        
    };
    
    

    
    /**
     * 
     */
    const preUninstall =
    exports.preUninstall = function(pkgId) {
        
        const ret = {
            unmodified:0,
            modified:{},
            other_objects:{}
        };
        
        const processBod = function(bodId) {
            let myClass = _.get(db[bodId], '_bo_meta_data.class_name');
            return db[bodId].find({},{_id:1}).then(objList=>{
                ret.other_objects[myClass] = _.map(objList, '_id');
            });
        };
        
        const processClass = function(className, idVerMap) {
            const isBod = (className === 'BusinessObjectDef');
            
            return db[className].find({_id:{$in:Object.keys(idVerMap)}}, {__ver:1}).then(objList=>{
                
                let bodPromises = [];
                
                _.forEach(objList, obj=>{
                    let manifestVer = new VersionId(idVerMap[obj._id]);
                    let objVer = new VersionId(obj.__ver);
                    let cmp = objVer.relationshipTo(manifestVer);
                    if(cmp.same) {
                        ret.unmodified++;
                    }
                    else {
                        ret.modified[className] = ret.modified[className] || [];
                        ret.modified[className].push(obj._id);
                    }
                    
                    if(isBod) {
                        bodPromises.push(processBod(obj._id));
                    }
                    
                });
                
                
                return Q.all(bodPromises);
                
            });
        };
        
        return db.BusinessObjectPackage.findOne({_id:pkgId}).then(bop=>{
            const promChain = new PromiseUtil.Chain();
            
            _.forEach(bop.manifest, (idVerMap, className)=>{
                if(className.indexOf('_') === 0) return;
                promChain.add(processClass.bind(null, className, idVerMap));
            });
            
            return promChain.promise.then(()=>ret);
        });
    };
	    
	/**
     * 
     */
    exports.uninstall = function(pkgId, keepData) {
        
        
        return db.BusinessObjectPackage.findOne({_id:pkgId}).then(bop=>{
            const promChain = new PromiseUtil.Chain();
            
            if(!keepData) {
                let bodMap = _.get(bop, 'manifest.BusinessObjectDef');
                if(bodMap) {
                    Object.keys(bodMap).forEach(bodId=>{
                        promChain.addInvoker(db[bodId], 'remove', [{}]);
                    });
                }
            }
            
            _.forEach(bop.manifest, (idVerMap, className)=>{
                if(className.indexOf('_') === 0) return;
                
                promChain.addInvoker(db[className], 'remove', [{_id:{$in:Object.keys(idVerMap)}}]);
            });
            
            return promChain.promise
            .then(()=>{
                return Config.getValue(BUILD_CFG_KEY, {}).then(paramVal=>{
                    if(paramVal[bop.key]) {
                        delete paramVal[bop.key];
                        if(paramVal._enabled === bop.key) {
                            paramVal._enabled = false;
                        }
                        return Config.saveValue(BUILD_CFG_KEY, paramVal);
                    }
                });
            })
            .then(()=>{
                return db.BusinessObjectPackage.remove({_id:pkgId});
            });
        });
    };
	
	return exports;
}