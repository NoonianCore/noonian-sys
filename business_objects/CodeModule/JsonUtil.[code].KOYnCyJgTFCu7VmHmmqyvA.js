function() {
    const exports = {};
    
    //library to allow stringified JSON to have consistent ordering of keys
    const jsonStringify = require('json-stable-stringify'); 
    
    exports.stableStringify = function(obj, indent) {
        indent = indent || '\t';
        return jsonStringify(obj, {space:indent});
    };
    
    
    exports.stringify = function(obj) {
        return JSON.stringify(obj, null,3);
    };
    
    
    return exports;
}