function(db, Q) {
    
    const exports = {};
    
    /**
     *  Remove UpdateLogs that are not awaiting incorporation into a package
     */
    const purgeUpdateLog = 
    exports.purgeUpdateLog = function() {
        return db.UpdateLog.remove({
            $or:[
                {incorporated:{$exists:true}},
                {package:null}
            ]
        });
    };
    
    /**
     * Purge all PackageConflict records
     */
    const purgePackageConflicts =
    exports.purgePackageConflicts = function() {
        return db.PackageConflict.remove({});
    };
    
    
    exports.cleanupDatabase = function() {
        return Q.all([purgeUpdateLog(), purgePackageConflicts()])
    }
    
    return exports;
    
}