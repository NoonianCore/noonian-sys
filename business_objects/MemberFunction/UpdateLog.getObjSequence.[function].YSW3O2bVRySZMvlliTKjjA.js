function(db, _, Q, PromiseUtil) {
    
    //Get full sequence of updates
    const getObjSequence = function(createUl) {
        const resultArr = [];
        return Q.all([
            db.BondProfile.findOne({_id:createUl.object_id}),
            db.UpdateLog.find({object_id:createUl.object_id}).sort({timestamp:'asc'})
        ])
        .spread((curr, updateSeq)=>{
            const promChain = new PromiseUtil.Chain();
            _.forEach(updateSeq, ul=>{
                promChain.addInvoker(ul, 'revert', [false]);
                promChain.add(v=>resultArr.push(v));
            });
            promChain.add(()=>{
                resultArr.push(curr); 
                return {objSeq:resultArr, ulSeq:updateSeq};
            });
            
            return promChain.promise;
        });
    };
    
    const generateSummary = function(objSeq, ulSeq) {
        if(!seq || !seq.length) return {};
        
        const summarySeq = [];
        
        const fieldList = Object.keys(seq[0]._bo_meta_data.type_desc_map);
                
        let anySubtractions = false, anyAdditions = false;
        
        for(let i=1; i < seq.length; i++) {
            let ul = ulSeq[i];
            let pre = seq[i-1];
            let post = seq[i];
            
            let subtractions = [];
            let additions = [];
            
            for(let f of bpFields) {
                let preVal = pre[f];
                let postVal = post[f];
                
                let preEmpty = preVal instanceof Array ? !preVal.length : !preVal;
                let postEmpty = postVal instanceof Array ? !postVal.length : !postVal;
                
                if(preEmpty && !postEmpty) {
                    additions.push(f);
                    anyAdditions = anyAdditions || {};
                    anyAdditions[f] = true;
                }
                if(!preEmpty && postEmpty) {
                    subtractions.push(f);
                    anySubtractions = anySubtractions || {};
                    anySubtractions[f] = true;
                }
                
            } //end field iteration
            
            summarySeq.push({ts:ul.timestamp, added:additions, removed:subtractions, ul:ul._id});
            
        } //end seq iteration
        
        
    };
    
    return function(className, id, opts) {
        opts = opts || {};
        const queryObj ={
            update_type:'create',
            object_class:className,
            object_id:id
        };
        
        return db.UpdateLog.findOne(queryObj).then(createUl=>{
            if(!createUl) {
                throw new Error(`Creation UpdateLog not found for ${className} ${id}`);
            }
            
            return getObjSequence(createUl);
            
        }).then(objSeqResult=>{
            if(!opts.summary) {
                return objSeqResult.objSeq;
            }
            else {
                return generateSummary(objSeqResult.objSeq, objSeqResult.ulSeq);
            }
        });
    }
}