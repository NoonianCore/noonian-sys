function (outStream) {
    //If outStream is an http response, set content-type appropriately
    if(typeof outStream.type === 'function') {
        outStream.type('css');
    }
    
    var content;
    if(this.preprocessor !== 'css' && this.compiled_content) {
        content = this.compiled_content;
    }
    else {
        content = this.content;
    }
    outStream.write(content);
    return outStream.end();
}