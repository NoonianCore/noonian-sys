function(db, PromiseUtil, _, Q) {
    
    return function() {
        const manifest = this.manifest;
        const result = {};
        const processBo = (className, id, manifestVersion)=>{
            const BoModel = db[className];
            if(!BoModel) {
                return Q(null);
            }
            return BoModel.findOne({_id:id}).then(bo=>{
                if(!bo) {
                    console.error('Bad id in manifest: %s.%s', className, id);
                    return;
                }
                var ret = {
                    _id:id,
                    display:bo._disp,
                    currentVersion:bo.__ver,
                    manifestVersion
                };
                if(bo.__ver !== manifestVersion) {
                    ret.version_mismatch = true;
                }
                result[className] = result[className] || [];
                result[className].push(ret);
            });
        };
        
        const promChain = new PromiseUtil.Chain();
        
        _.forEach(manifest, (idVerMap, boClass)=>{
            _.forEach(idVerMap, (ver, id)=>{
                promChain.add(processBo.bind(null, boClass, id, ver));
            });
        });
        
        
        return promChain.promise.then(()=>{
            const ret = {};
            _.forEach(result, (list, className) =>{
                ret[className] = _.sortBy(list, 'display');
            });
            return ret;
        });
    };
}