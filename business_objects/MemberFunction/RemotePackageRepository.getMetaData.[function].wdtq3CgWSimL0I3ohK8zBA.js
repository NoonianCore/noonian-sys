function(Q) {
    
    const request = require('request');
    
    return function() {
        
		const deferred = Q.defer();

        const rpr = this;
        
		if(!rpr || !rpr.url) {
			deferred.reject('Bad RemotePackageRepository entry');
			return deferred.promise;
		}
		
		var auth;
		if(rpr.auth && rpr.auth.token) {
		    auth = {bearer:rpr.auth.token};
		}
		
		const fullUrl = `${rpr.url}/ws/package_repo/getMetaData`;
		
		request(
			{
				method:'GET',
				uri:fullUrl,
				auth,
				json:true
			},
			function(err, httpResponse, body) {
				if(err || body.error) {
				    console.error(err);
					return deferred.reject(err || body.error);
				}

				deferred.resolve(body);
			}
		);

		return deferred.promise;
	};
}