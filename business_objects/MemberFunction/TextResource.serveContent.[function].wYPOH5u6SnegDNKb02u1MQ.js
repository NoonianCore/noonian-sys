function (outStream) {
    //If outStream is an http response, set content-type appropriately
    if(typeof outStream.type === 'function' && this.content_type) {
        outStream.type(this.content_type);
    }
    
    outStream.write(this.content);
    return outStream.end();
}