function(db, _, Q, nodeRequire) {
    
    const diffTool = nodeRequire('../api/datasource/packaging/diffpatch');
    
    return function(saveTarget) {
        return Q.all([
            db.UpdateLog.find({object_id:this.object_id, timestamp:{$gt:this.timestamp}}).sort({timestamp:'desc'}),
            db[this.object_class].findOne({_id:this.object_id})
        ])
        .spread((ulList, targetObj)=>{
            
            //Apply patches in order of newest to oldest
            _.forEach(ulList, currUl=>{
                if(currUl.revert_patch) {
                    diffTool.patch(targetObj, currUl.revert_patch);
                }
            });
            
            if(this.revert_patch) {
                diffTool.patch(targetObj, this.revert_patch);
            }
            
            if(saveTarget) {
                return targetObj.save();
            }
            
            return targetObj;
        });
    }
}