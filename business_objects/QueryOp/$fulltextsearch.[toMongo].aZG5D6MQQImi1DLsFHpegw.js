function (searchStr, typeDescMap, FieldTypeService) {
    
    if(searchStr) {
    
        if(searchStr.indexOf('=') === 0) {
            searchStr = searchStr.substring(1);
        }
        else {
            let parts = searchStr.match(/(?:[^\s"]+|"[^"]*")+/g); //Split terms; allow for quoted terms
            let fixed = [];
            parts.forEach(p=>{
                if(p.charAt(0) == '"' && p.charAt(p.length-1) == '"') {
                    p = p.substring(1, p.length-1);
                }
                p = p.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
                fixed.push(p);
            });
            searchStr = fixed.join('|');
        }
    
    }
    
    var retCond = {$or:[
        {__disp:{$regex:searchStr, $options:'i'}},
        {__match_text:{$regex:searchStr, $options:'i'}}
    ]};
    var condList = retCond.$or;
    
    const processTd = function(tdm, prefix) {
        prefix = prefix || '';
        for(var fieldName in tdm) {
            var td = tdm[fieldName];
            if(fieldName.indexOf('_') === 0)
                continue;
            
            if(td instanceof Array) {
                td = td[0];
            }
    
            var mongoType = FieldTypeService.getSchemaElem(td);
    
            if(!mongoType) {
            //   console.log("No typeMapper for "+td.type+" field "+fieldName);
              continue;
            }
            var newCond;
            if(mongoType.textIndex) {
                newCond = {};
                newCond[prefix+fieldName] = {$regex:searchStr, $options:'i'};
                condList.push(newCond);
            }
            else if(td.type === 'reference') {
                newCond = {};
                newCond[prefix+fieldName+'._disp'] = {$regex:searchStr, $options:'i'};
                condList.push(newCond);
            }
            else if(td.type === 'composite' && td.type_desc_map) {
                processTd(td.type_desc_map, prefix+fieldName+'.');
            }
    
        }
    }
    
    processTd(typeDescMap);

    
    return retCond;
}