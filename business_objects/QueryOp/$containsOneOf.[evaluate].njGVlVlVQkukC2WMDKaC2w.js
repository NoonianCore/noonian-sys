function (expressionValue, testValue) {
    //testValue - comes from the field of the business object; array of references
    //expressionValue - comes from expression being evaluated; array of references OR just id's
    
    
    if(!expressionValue || !expressionValue.length) {
        return false;
    }
    
    if(testValue && testValue instanceof Array) {
        for(var i=0; i < testValue.length; i++) {
            for(var j=0; j < expressionValue.length; j++) {
                let testId = testValue[i] && testValue[i]._id || testValue[i];
                let expId = expressionValue[j] && expressionValue[j]._id || expressionValue[j];
                if(testId === expId) {
                    return true;
                }
            }
        }
    }   
    return false;
}