function (fieldName, clause) {
    var plusDays = +clause;
    var d = new Date();
    
    d.setDate(d.getDate()+plusDays);
    
    var ret = {};
    var parsed = /(\d{4}-\d{2}-\d{2}).*/.exec(d.toISOString());
    if(parsed && parsed[1]) {
        var day = parsed[1];
        
        ret[fieldName] = {$lte:day};
    }
    
    return ret;
}