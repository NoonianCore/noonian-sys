function (expressionValue) {
    if(expressionValue && expressionValue._disp)
        return 'does not contain '+expressionValue._disp;
    else 
        return 'does not contain '+expressionValue;
}