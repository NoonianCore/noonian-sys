function (expressionValue, testValue) {
    //testValue is after expressionValue?
    var plusDays = +expressionValue;
    var expDate = new Date();
    
    expDate.setDate(expDate.getDate()+plusDays);
    
    var testDate = testValue;
    if(!(testDate instanceof Date)) {
        testDate = new Date(testValue);
    }
    
    return testDate.getTime() < expDate.getTime();
}