function (fieldName, clause) {
    var result = {};
    var idList = [];
    //Clause may be list of ids or list of references
    if(clause && clause.length) {
        for(var i=0; i < clause.length; i++) {
            if(clause[i]) {
                idList.push(clause[i]._id || clause[i]);
            }
        }
    }
    
    
    result[fieldName+'._id'] = {$in:idList};
    
    return result;
}