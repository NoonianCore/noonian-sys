function (expressionValue, testValue) {
    if(testValue) {
        return new RegExp(testValue).test(expressionValue);
    }
    else {
        return false;
    }
}